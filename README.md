# Getting Started

Remember you need to wait for your site to build before you will be able to see your changes.  You can track the build on the **Pipelines** tab.

1. Create repository to match the name you want for your site (e.g. <your_git_id>.gitlab.io)
2. **IMPORTANT:** If you forking, remove the fork relationship.
Go to **Settings (⚙)** > **Edit Project** and click the **"Remove fork relationship"** button.
3. Edit your website through GitLab or clone the repository and push your changes.

## site setting
1. edit  `_config.yml` to match your config

## posting

Adding a new post, which is the most important part, is as simple as adding a new file to the folder `_post`. Name the new file something like `2022-02-10-from-zero-to-hero.md`. The pattern here is the date of the post in ISO format, i.e. YYYY-MM-DD with the month coming second. As for the post itself:

```
---
layout: post
title:  "Peeking into Tensorflow-Keras"
date:   2023-11-01 11:32:14 -0800
categories: deep learning
---

```