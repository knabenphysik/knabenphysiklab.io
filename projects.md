---
layout: page
title: Projek Dulu-dulu
permalink: /projects/
---

* [Twistcode Internal Benchmark with Graphcore, Dec 2022](https://github.com/twistcode/graphcore_benchmark): Conducted internal benchmark for deep learning inferencing

* [Twistcode HPCG Benchmark, June 2019](https://github.com/twistcode/hpcg) (contributor): facilitated in server clustering

* [Twistcode Deep Learning Technology, 2015 - 2020](https://github.com/twistcode) (contributor): from [image classification/detection](https://twistcode.com/services/mata/) to [time-series prediction](https://twistcode.com/services/okane/)

* [Extol MSC Bhd, June 2010, LogAbout mapping of Self Quotient Image](https://ieeexplore.ieee.org/document/5518582) (contributor): Face feature extraction for face verification

* [Extol MSC Bhd, 2009, Face Verification System](https://www.appasia.com/wp-content/uploads/2024/03/2008.pdf) (contributor): Neural Network based Face Verification System