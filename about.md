---
layout: page
title: Aku
permalink: /about/
---

## ...this site

***Writing forces sharper understanding.***

*Telling stories is a way of surviving and continuing to live* 
<p>- <a href="https://www.nobelprize.org/prizes/literature/2006/pamuk/25276-interview-transcript-2006/">Orhan Pamuk</a> </p>


... a place for me to share ideas and things I've worked on, and in the process of doing so, build a better understanding of the underlying principles or technologies at play.

## ...me

<br>I’m currently working as a Machine Learning Engineer at <a href="https://htasia.com/">HT Consulting</a> in Petaling Jaya (Malaysia).<br><br>