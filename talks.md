---
layout: page
title: Sembang
permalink: /talks/
---


*   Job Readiness Programme: AI 101
    * Presented at [HT Consulting (Asia)] - June 2024
        * Lecture 01 available [here](https://knabenphysik.gitlab.io/jrp_note/The-Beauty-of-Learning-Introduction.html)
        * Lecture 02 available [here](https://knabenphysik.gitlab.io/jrp_note/Life-Cycle-and-Data.html)
        * Lecture 03 available [here](https://knabenphysik.gitlab.io/jrp_note/eda.html)
        * Lecture 04 available [here](https://knabenphysik.gitlab.io/jrp_note/learning.html)
        * Lecture 05 available [here](https://knabenphysik.gitlab.io/jrp_note/evaluation.html)
        * Lecture 06 available [here](https://knabenphysik.gitlab.io/jrp_note/machine_learning.html)
        * Practical code example [here](https://gitlab.com/knabenphysik/jrp_note/-/tree/main/practical_code?ref_type=heads)
        * Theory Assessment [here](https://forms.gle/Tp2NQiYsqEjyo9fa7) & Practical Assessment [here](https://gitlab.com/knabenphysik/jrp_note/-/blob/main/exam/AI_practical.ipynb)
        * Practical Assessment Solution : [Q1](https://gitlab.com/knabenphysik/jrp_note/-/blob/main/exam/cifar_100_solution.ipynb), [Q2](https://gitlab.com/knabenphysik/jrp_note/-/blob/main/exam/fruit_solution.ipynb) and [Q3](https://gitlab.com/knabenphysik/jrp_note/-/blob/main/exam/potato_solution.ipynb)

*   Industrial Talk: Machine Learning & Physics
    * Presented at [Computational Physics Class, Jabatan Fizik UM] - 10 June 2021
    * Slide available [here](../talk/ML_n_Physics.pdf)

*   Deep Learning Roadmap with A.D.A.M Supercomputer
    * Presented at [Twistcode-Day, Zenith Hotel, Putrajaya] - 29 November 2018
    * Slide available [here](../talk/Twistcode_Deep_Learning_2018.pdf)       

*   Career Talk: From Physics to IT
    * Presented at [Physics Colloquium 2014, Jabatan Fizik UM] - 2014