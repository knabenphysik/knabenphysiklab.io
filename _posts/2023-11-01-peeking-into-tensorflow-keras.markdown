---
layout: post
title:  "Peeking into Tensorflow-Keras"
date:   2023-11-01 11:32:14 -0800
categories: deep learning
---

# Introduction

Machine learning algorithms can be broadly categorized as **_unsupervised_** or **_supervised_** by what kind of "experience" they are allowed to have during the learning process. In these case, "experience" is called **dataset**. Sometimes we call them **data points**. Unsupervised learning algorithms experience a dataset containing many features, then learn useful properties of the structure of this dataset. Supervised learning algorithms experience a dataset containing features, but each example is also associated with a label or target.

## Neural Network

<div style="text-align:center;"><img src="{{ site.baseurl }}{% link images/2023-11-01-peeking-into-tensorflow-keras/MLP.png %}" alt="Depiction of ANN"></div>

Neural networks or connectionist architectures provide an alternative computational paradigm, and can be seen as a step towards the understanding of intelligence. It departs from the traditional von Neumann serial processing and instead is based on distributed processing via connections between simple elements.

The goal of a neural network is to **_approximate some function_** by learning parameters that results in the best approximation. Another way of saying this is **_minimising_** the difference (loss function is used to perform the estimation) between the **_expected output_** and the **_actual_** one.

## Modelling

Models are abstractions of reality to which experiments can be applied to improve our understanding of phenomena in the world. They are at the heart of science in which models can be used to process data to predict future events or to organise data in ways that allow information to be extracted from it. There are two common approaches to constructing models. 

The first is of a deductive nature. It relies on subdividing the system being modelled into subsystems that can be expressed by accepted relationships and physical laws. These subsystems are typically arranged in the form of simulation blocks and sets of differential equations. The model is consequently obtained by combining all the sub-models.

The second approach favours the inductive strategy of estimating models from measured data. This estimation process will be referred to as _"learning from data"_ or simply _"learning"_ for short. 

In general, a neural network consists of layers of neurons where each neuron computes the following activation function:

$$
f(x) = \phi(\mathbf{w}^Tx+b)
$$

where $x$ is the input to the neuron, $w$ is a weight vector, $b$ is a bias term and $\phi$ is a nonlinearity function. Each neuron receives potentially many inputs, and outputs a single number. The nonlinearity is important because it allows layers of neurons to learn non-linear functions. In these layered structures, the output of one layer of units becomes the inputs to the next layer of units. 

We need to find the weights and biases so that the outputs of the net comes as close as possible to their true values. Since we know that _**loss function**_ will be used to measure this _close value_, adjustment values of _**weights and biases**_ is via optimizer.

## Tensor

Mathematically, a tensor is a generalization of vector and matrices. It the context of **_Tensorflow_**, a tensor is considered as a multidimensional array.

<div style="text-align:center;"><img src="{{ site.baseurl }}{% link images/2023-11-01-peeking-into-tensorflow-keras/concept_tf.webp %}" alt="Depiction of tensor: tensor visualization 1"></div>

<div style="text-align:center;"><img src="{{ site.baseurl }}{% link images/2023-11-01-peeking-into-tensorflow-keras/concept_tf2.jpg %}" alt="tensor visualization 1"></div>

# Tensorflow

- Tensorflow 2.x has adopted **keras API** as standard method writing neural network
- Tensorflow 2.x use **_eager execution_** by default

When writing a TensorFlow program, the main **_object_** that is manipulated and passed around is the **_tf.Tensor_**. TensorFlow supports **_eager execution_** and _**graph execution**_. In eager execution, operations are evaluated immediately. In graph execution, a computational graph is constructed for later evaluation.

**_tf.Tensor_** computation is accelerated via GPU's, TPU's!

<div style="text-align:center;"><img src="{{ site.baseurl }}{% link images/2023-11-01-peeking-into-tensorflow-keras/tf_arch.jpeg %}" alt="Where Keras-Tensorflow fit in?"></div>

<div style="text-align:center;"><img src="{{ site.baseurl }}{% link images/2023-11-01-peeking-into-tensorflow-keras/tf_arch.png %}" alt="Where Keras-Tensorflow fit in?"></div>

## Available optimizers in Tensorflow 

- [Stochastic gradient descent (SGD)](https://keras.io/api/optimizers/sgd/)
- RMSprop
- Adam
- AdamW
- Adadelta
- Adagrad
- Adamax
- Adafactor
- Nadam
- Ftrl

<div style="text-align:center;"><img src="{{ site.baseurl }}{% link images/2023-11-01-peeking-into-tensorflow-keras/optimizer_compare.png %}" alt="Comparison of different optimizer"></div>

## Available loss function in Tensorflow

**Probabilistic losses**

- BinaryCrossentropy class
- CategoricalCrossentropy class
- SparseCategoricalCrossentropy class
- Poisson class
- binary_crossentropy function
- categorical_crossentropy function
- sparse_categorical_crossentropy function
- poisson function
- KLDivergence class
- kl_divergence function

**Regression losses**

- MeanSquaredError class
- MeanAbsoluteError class
- MeanAbsolutePercentageError class
- MeanSquaredLogarithmicError class
- CosineSimilarity class
- mean_squared_error function
- mean_absolute_error function
- mean_absolute_percentage_error function
- mean_squared_logarithmic_error function
- cosine_similarity function
- Huber class
- huber function
- LogCosh class
- log_cosh function

more [here](https://keras.io/api/losses/)

<div style="text-align:center;"><img src="{{ site.baseurl }}{% link images/2023-11-01-peeking-into-tensorflow-keras/learning_rate.webp %}" alt="loss function minimization via learning rate"></div>


# Tensorflow in Action

## Data preparation

- read image using OpenCV
- read image using [Pillow](https://pillow.readthedocs.io/en/stable/)
- read image using [tf.keras.utils](https://www.tensorflow.org/api_docs/python/tf/keras/utils)

Data usually formatted in 3-dimension : **(60000, 28, 28)**

This data images is stored in a 3D tensor of axes 3 and having shape representing 60,000 matrices of 28×28 integers. 

<div style="text-align:center;"><img src="{{ site.baseurl }}{% link images/2023-11-01-peeking-into-tensorflow-keras/3-axis_front.png %}" alt="Data tensor visualize"></div>

## Neural Network Stacking

**Multi Layer Perceptron**

<div style="text-align:center;"><img src="{{ site.baseurl }}{% link images/2023-11-01-peeking-into-tensorflow-keras/simple_mlp.png %}" alt="Simple MLP"></div>

```python
model_2 = tf.keras.models.Sequential(name="simple-MLP")
model_2.add(tf.keras.layers.Dense(2, input_shape = (1,)))
model_2.add(tf.keras.layers.Dense(1, activation='sigmoid'))
```

**MLP with Feature Extraction** 

```python
model = tf.keras.models.Sequential(name="simple-CNN")
model.add(tf.keras.layers.Conv2D(filters = 32, kernel_size = (5, 5), activation='relu', padding='same', input_shape = (IMG_SIZE,IMG_SIZE,1)))
model.add(tf.keras.layers.MaxPooling2D(pool_size = (2, 2)))

model.add(tf.keras.layers.Flatten())
model.add(tf.keras.layers.Dense(128))
model.add(tf.keras.layers.Activation('relu'))
model.add(tf.keras.layers.Dense(3, activation='softmax'))
```

**Simple Autoencoder**

```python
input_layer = keras.Input(shape=(height, width, 1))
# encoding
x = keras.layers.Conv2D(64, (3, 3), activation='relu', padding='same')(input_layer)
x = keras.layers.Conv2D(128, (3, 3), activation='relu', padding='same')(x)
x = keras.layers.BatchNormalization()(x)
x = keras.layers.MaxPooling2D((2, 2), padding='same')(x)
x = keras.layers.Dropout(0.5)(x)

# decoding
x = keras.layers.Conv2D(128, (3, 3), activation='relu', padding='same')(x)
x = keras.layers.Conv2D(64, (3, 3), activation='relu', padding='same')(x)
x = keras.layers.BatchNormalization()(x)
x = keras.layers.UpSampling2D((2, 2))(x)

output_layer = keras.layers.Conv2D(1, (3, 3), activation='sigmoid', padding='same')(x)

model = tf.keras.Model(inputs=[input_layer], outputs=[output_layer])
```

When we are dealing with network that has **feature extraction**, convolution operation is used.

<div style="text-align:center;"><img src="{{ site.baseurl }}{% link images/2023-11-01-peeking-into-tensorflow-keras/cnn_1.png %}" alt="convolve stride with no padding MLP"></div>

<div style="text-align:center;"><img src="{{ site.baseurl }}{% link images/2023-11-01-peeking-into-tensorflow-keras/cnn_2.png %}" alt="convolve stride with padding"></div>

## Training

<div style="text-align:center;"><img src="{{ site.baseurl }}{% link images/2023-11-01-peeking-into-tensorflow-keras/geo_error.png %}" alt="Geometrical view of loss function over weight space"></div>

<div style="text-align:center;"><img src="{{ site.baseurl }}{% link images/2023-11-01-peeking-into-tensorflow-keras/error.png %}" alt="3D view of loss function over weight space"></div>

Depiction of how $E(w)$ takes its smallest value
:::

**Batch size** defines the **_number of samples we use in one epoch_** to train a neural network. There are three types of gradient descent in respect to the batch size:

- Batch gradient descent – uses all samples from the training set in one epoch.
- Stochastic gradient descent – uses only one random sample from the training set in one epoch.
- Mini-batch gradient descent – uses a predefined number of samples from the training set in one epoch.

<div style="text-align:center;"><img src="{{ site.baseurl }}{% link images/2023-11-01-peeking-into-tensorflow-keras/bat_learning.png %}" alt="3D view of loss function over weight space"></div>

# Best Practice

| Problems    | Output Layer | Loss |
| ----------- | ----------- | ----------- |
| Regression  | tf.keras.layers.dense(1, activation = 'linear' | mean_square_error |
| Binary classification  | tf.keras.layers.dense(1, activation = 'sigmoid' | binary_crossentropy |
| Multi-label classification (predict best label only)  | tf.keras.layers.dense(10, activation = 'softmax' | sparse_categorical_crossentropy |
| Multi-label classification (multiple correct answers)  | tf.keras.layers.dense(10, activation = 'sigmoid' | categorical_crossentropy |